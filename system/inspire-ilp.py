#!/usr/bin/python2.7

# Inspire Inductive Logic Programming System
# Copyright (C) 2016-2018  Peter Schueller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, re, tempfile, subprocess, os, json, traceback, random, collections

# env

root = os.path.dirname(sys.argv[0])
if root == '':
  root = os.curdir

CLINGO = 'clingo'

# config
SOLVERARGS = ''
DEFAULTINPUTFILE = root+'/../examples/simple.task'
MINCOSTLIMIT = 4
MAXCOSTLIMIT = 15
SAFETYMARGIN = 11

HYPGENLP = root+'/hypgen.lp'

PRINTHYPOTHESIS = True
VERBOSE = True
DEBUG = False
RMTEMPS = True

# higher level = optimized first
WEAK_COST_EX = 1
WEAK_LVL_EX = 2

WEAK_COSTMUL_HYP = 1
WEAK_LVL_HYP = 1

# regexes

rTimeDotsFact = re.compile(r'\s*time\(\s*([0-9]+)\s*\.\.\s*([0-9]+)\s*\).\s*')
rTimeFact = re.compile(r'\s*time\(\s*([0-9]+)\s*\).\s*')
rTimedAtom = re.compile(r'^([a-z][a-zA-Z_]*)\s*\(\s*(\(\s*[0-9]+\s*,\s*[0-9]+\s*\))\s*,\s*([0-9]+)\s*\)$')
rSymbolicAtom1 = re.compile(r'^([a-z][a-zA-Z_]*)\s*\(\s*([a-z][a-zA-Z_]*)\s*\)$')
rSymbolicAtom2 = re.compile(r'^([a-z][a-zA-Z_]*)\s*\(\s*([a-z][a-zA-Z_]*)\s*,\s*([a-z][a-zA-Z_]*)\s*\)$')
rUseHR = re.compile(r'^usehr\(([0-9]+)\)$')

def warn(msg):
  sys.stderr.write(str(msg)+'\n')

def verbose(msg):
  if VERBOSE:
    sys.stderr.write(str(msg)+'\n')

def debug(msg):
  if DEBUG:
    sys.stderr.write(str(msg)+'\n')

def info(msg):
  sys.stdout.write(str(msg)+'\n')
  #sys.stderr.write(str(msg)+'\n')

def debugTask(task):
  # debug output
  for section in ['background', 'relevant_predicates', 'target_predicate']:
    debug('***{}***'.format(section))
    debug('  '+'\n  '.join(task[section]))
  for section in ['examples', 'tests']:
    debug('***{}***'.format(section))
    for ex in task[section]:
      debug('  #{}'.format(ex['id']))
      assert(ex['start'] == [])
      #debug('    TRACE\n      '+'\n      '.join(ex['trace']))
      debug('    TRACE '+' '.join(ex['trace']))
      if 'valid_moves' in ex:
        #debug('    VALID\n      '+'\n      '.join(ex['valid_moves']))
        debug('    VALID '+' '.join(ex['valid_moves']))

def readTask(infile):
  location = 'start'
  task = {
    'probabilistic' : False,
    'start' : [], # should stay empty
    'background' : [],
    'target_predicate' : [],
    'relevant_predicates' : [],
    'examples' : [],
    'tests' : [],
  }

  rRelPred = re.compile('#relevant_predicates (.*)')
  rExample = re.compile('#Example\((.*)\)')
  rTest = re.compile('#Test\((.*)\)')
  with open(infile, 'r') as inf:
    for fullline in inf:
      line = fullline.strip()
      if line == '':
        pass
      elif line == '#probabilistic':
        # Note that you only need to check this if your system is entering both
        # tracks (otherwise, you can assume you are only given tasks from your own
        # track)
        task['probabilistic'] = True
      elif line == '#background':
        location = 'background'
      elif line.startswith('#target_predicate'):
        location = 'target_predicate'
      elif line.startswith('#relevant_predicates'):
        location = 'relevant_predicates'
        rel = rRelPred.findall(line)
        #warn(repr('REL'+repr(rel)))
      elif line.startswith('#Example'):
        location = 'examples'
        sublocation = 'start'
        exid = rExample.findall(line)
        #warn(repr('EX'+repr(exid)))
        task['examples'].append({
          'id': exid[0],
          'start': [], # should stay empty
          'trace': [],
          'valid_moves': [],
        })
      elif line.startswith('#trace'):
        sublocation = 'trace'
      elif line.startswith('#valid_moves') and location == 'examples':
        sublocation = 'valid_moves'
      elif line.startswith('#Test'):
        location = 'tests'
        sublocation = 'trace'
        testid = rTest.findall(line)
        task['tests'].append({
          'id' : testid[0],
          'start': [], # should stay empty
          'trace' : [],
        })
      else:
        if location == 'tests' or location == 'examples':
          #warn('appending to '+repr(task[location][-1][sublocation]))
          #warn('appending '+repr(line))
          task[location][-1][sublocation].append(line.strip('.'))
        else:
          task[location].append(line)

  assert(not task['probabilistic'])
  # for probabilistic:
  # puts "VALID(#{trace[:id]}, #{rand(101).to_f/100.0})"

  # otherwise there is junk at the beginning
  assert(task['start'] == [])

  # parser assertion
  for section in ['examples', 'tests']:
    for ex in task[section]:
      assert(ex['start'] == [])
  assert(task['start'] == [])

  return task

def transformTimedAtom(atom, add):
  # parse atom
  m = rTimedAtom.match(atom)
  assert(m)
  pred, cell, time = m.groups()
  # output with increased time
  return '{}({},{})'.format(pred, cell, int(time)+add)

def getAnswersets(program, args='0', withCost=False):
  '''
  default arguments: get all answer sets
  '''
  #warn('getAnswersets with program:\n***\n{}\n***'.format(program))
  tf = tempfile.NamedTemporaryFile(prefix='insplp',delete=False)
  tf.write(program)
  tf.close()
  warn('wrote ASP to {}'.format(tf.name))
  clingoproc = subprocess.Popen('{} {} --outf=2 {} {}'.format(CLINGO, SOLVERARGS, args, tf.name), shell=True, stdin=None, stdout=subprocess.PIPE)
  outs, err = clingoproc.communicate(None)
  if RMTEMPS:
    os.remove(tf.name)
  #warn('FROMCLINGO:\n'+outs)
  out = json.loads(outs)
  if 'Witnesses' in out['Call'][0]:
    answersets = out['Call'][0]['Witnesses']
    if 'Optimal' in out['Models']:
      # if we have N optimal models, these are the N last witnesses
      answersets = answersets[-out['Models']['Optimal']:]
    #warn('FROMCLINGO:\n'+repr(out))
    if withCost:
      return [ (witness['Value'], witness['Costs']) for witness in answersets]
    else:
      return [witness['Value'] for witness in answersets]
  else:
    return []

def attemptPredict(infostr, task, hypothesis):
  hypstr = hypothesis2str(hypothesis, indent='  ')
  debug('prediction attempt {} with hypothesis:\n{}'.format(infostr, hypstr))
  # You can do this multiple times. Only your final attempt will be scored.
  info("#attempt")

  for test in task['tests']:
    traceid = test['id']
    trace = test['trace']

    testprog = []
    # copy background into testprog, do not modify background!
    testprog += task['background']
    testprog.append(hypstr)
    # trace
    testprog.append('\n'.join(map(lambda t: t+'.', trace)))
    # test trace
    testprog.append('invalid_at(Time) :- agent_at(Cell,Time), not valid_move(Cell,Time-1), time(Time-1), time(Time).')
    testprog.append('invalid :- invalid_at(Time).')
    testprog.append('#show invalid/0.')

    testprogs = '\n'.join(testprog)
    #warn('evaluating trace {} in testprog\n***\n{}\n***'.format(traceid, testprogs))
    answersets = getAnswersets(testprogs, args='1')
    assert(len(answersets) > 0)
    first = answersets[0]

    if 'invalid' in first:
      info("INVALID({})".format(traceid))
    else:
      info("VALID({})".format(traceid))

def verifyHypothesisOnExample(task, ex, hyp):
  hypstr = hypothesis2str(hyp, indent='  ')
  testprog = []
  # copy background into testprog, do not modify background!
  testprog += task['background']
  # hypothesis
  testprog.append(hypstr)
  # add trace
  testprog.append('\n'.join(map(lambda t: t+'.', ex['trace'])))

  # recognize coverage of example
  exid = ex['id']
  timeadd = 0
  transformedpositive = map(lambda vm: transformTimedAtom(vm, timeadd), ex['valid_moves'])
  testprog.append("covered_ex({}) :- {}.".format(exid, ', '.join(transformedpositive)))
  # recognize violation of example
  testprog.append(''.join(map(lambda x: 'pos_'+x+'.', transformedpositive)))
  testprog.append("violated_ex({}) :- valid_move(X,Y), not pos_valid_move(X,Y).".format(exid))
  # recognize quality of example
  testprog.append("good_ex(Ex) :- covered_ex(Ex), not violated_ex(Ex).")
  # forbid noncovered examples
  testprog.append(':- good_ex({}).'.format(exid))

  testprogs = '\n'.join(testprog)
  #warn('evaluating trace {} in testprog\n***\n{}\n***'.format(traceid, testprogs))
  # just get one answer set
  answersets = getAnswersets(testprogs, args='1')
  #warn('trace evaluation gave result {}'.format(repr(answersets)))
  if len(answersets) == 0:
    # no answer set was found -> all answer sets are good
    return True
  else:
    # at least one answer set with a bad case was found
    return False

hypothesesGenerationCache = {}
hypothesesGenerationCacheForTask = None
def generateHypotheses(task, costlimit):
  global hypothesesGenerationCache, hypothesesGenerationCacheForTask
  if hypothesesGenerationCacheForTask is None:
    hypothesesGenerationCacheForTask = task
  elif hypothesesGenerationCacheForTask != task:
    raise Exception('cannot change task for hypothesis generation cache!')
  if costlimit not in hypothesesGenerationCache:
    hypothesesGenerationCache[costlimit] = generateHypothesesInt(task, costlimit)
  return hypothesesGenerationCache[costlimit]

def generateHypothesesInt(task, costlimit):
  prog = []

  atm = None
  if len(task['target_predicate']) == 1:
    atm = task['target_predicate'][0]
  else:
    raise Exception('nonunique or missing target_predicate')
  m = rSymbolicAtom1.match(atm)
  if m:
    pred, arg1 = m.groups()
    prog.append('tpred(id_{},{},1).'.format(pred, pred))
    prog.append('targ(id_{},1,{}).'.format(pred, arg1))
  else:
    m = rSymbolicAtom2.match(atm)
    if m:
      pred, arg1, arg2 = m.groups()
      prog.append('tpred(id_{},{},2).'.format(pred, pred))
      prog.append('targ(id_{},1,{}).'.format(pred, arg1))
      prog.append('targ(id_{},2,{}).'.format(pred, arg2))
    else:
      raise Exception('unexpected target_predicate {}'.format(repr(modeh)))

  # modeb bias
  if len(task['relevant_predicates']) == 0:
     raise Exception('missing relevant_predicates')
  for atm in task['relevant_predicates']:
    m = rSymbolicAtom1.match(atm)
    if m:
      pred, arg1 = m.groups()
      prog.append('rpred(id_{},{},1).'.format(pred, pred))
      prog.append('rarg(id_{},1,{}).'.format(pred, arg1))
    else:
      m = rSymbolicAtom2.match(atm)
      if m:
        pred, arg1, arg2 = m.groups()[0], m.groups()[1], m.groups()[2]
        prog.append('rpred(id_{},{},2).'.format(pred, pred))
        prog.append('rarg(id_{},1,{}).'.format(pred, arg1))
        prog.append('rarg(id_{},2,{}).'.format(pred, arg2))
        extra = ''
        if pred in ['adjacent', 'wall']:
          prog.append('nonreflexive(id_{}).'.format(pred))
          prog.append('symmetric(id_{}).'.format(pred))
      else:
        raise Exception('unexpected relevant_predicate {}'.format(repr(modeb)))

  prog.append(open(HYPGENLP, 'r').read())
  hyp_as = getAnswersets('\n'.join(prog), args='--const maxcost={} 0'.format(costlimit))

  allrules = []
  for ras in hyp_as:
    if True:
      # nonstructured (for debugging)
      nsrule, nscost = getRuleFromAnswerset(ras, structured=False, debugCosts=False)
      warn("genhyp: {} [cost {}]".format(repr(nsrule), nscost))
    # structured (will be returned)
    rule, cost = getRuleFromAnswerset(ras, structured=True)
    #warn("GENHYP: {} [cost {}]".format(repr(rule), cost))
    allrules.append( (rule,cost) )
  debug("Generated {} hypothesis for costlimit {}".format(len(allrules), costlimit))
  return allrules

def parseTimes(background):
  times = set()
  for bg in background:
    #warn('parseTimes bg:'+repr(bg))
    m = rTimeDotsFact.match(bg)
    if m:
      #warn('parseTimes:'+repr(m.groups()))
      timelow, timeup = m.groups()
      times.add(int(timelow))
      times.add(int(timeup))
    m = rTimeFact.match(bg)
    if m:
      #warn('parseTimes:'+repr(m.groups()))
      time = m.groups()
      times.add(int(time))
  return min(times), max(times)

def makeRule(rule, addpbody=[]):
  lits = [ makeLiteral(lit, True) for lit in rule['pbody'] ] + addpbody
  lits += [ makeLiteral(lit, False) for lit in rule['nbody'] ]
  return makeAtom(rule['head']) + " :- " + ",".join(lits) + "."

def learn(task, examples, costlimit):
  assert(len(examples) == 1) # SAFETYMARGIN and so does not work (e.g., for visited/2 predicate)
  main = []
  for line in task['background']:
    # do not output time from background (will do per example)
    if not line.startswith('time('):
      main.append(line)

  # parse min/max time in background knowledge
  timelow, timeup = parseTimes(task['background'])

  # find safe distance between times
  timemult = SAFETYMARGIN+timeup - timelow

  ### add to background knowledge time for each example
  ##for ex in task['examples']:
  ##  exid = int(ex['id'])
  ##  main.append('time({}..{}).'.format(timelow+exid*timemult, timeup+exid*timemult))

  # output examples and corresponding traces with transformed times
  # (for xhail, examples are just normal background)
  for ex in examples:
    exid = int(ex['id'])
    timeadd = exid*timemult
    # trace (input)
    for t in ex['trace']:
      main.append(transformTimedAtom(t, timeadd)+'.')
    # recognize coverage of example
    transformedpositive = map(lambda vm: transformTimedAtom(vm, timeadd), ex['valid_moves'])
    main.append("covered_ex({}) :- {}.".format(exid, ', '.join(transformedpositive)))
    # recognize violation of example
    main.append(''.join(map(lambda x: 'pos_'+x+'.', transformedpositive)))
    main.append("violated_ex({}) :- valid_move(X,Y), not pos_valid_move(X,Y).".format(exid))
    # recognize quality of example
    main.append("good_ex(Ex) :- covered_ex(Ex), not violated_ex(Ex).")
    # optimize covered examples
    #main.append(':~ not good_ex({}). [{}@{},{}]'.format(exid, WEAK_COST_EX, WEAK_LVL_EX, exid))
    # require covered example
    main.append(':- not good_ex({}).'.format(exid))

  # use those times actually used in examples
  main.append('time(X) :- agent_at(_,X).')
  main.append('time(X) :- pos_valid_move(_,X).')

  # hypothesis
  hyp = generateHypotheses(task, costlimit)
  for hridx, hr in enumerate(hyp):
    #warn('hr: {}/{}'.format(hridx, repr(hr)))
    rule, cost = hr
    use_atom = 'usehr({})'.format(hridx)
    main.append('{{ {} }}.'.format(use_atom))
    main.append(':~ {}. [{}@{},{}]'.format(use_atom, cost*WEAK_COSTMUL_HYP, WEAK_LVL_HYP,hridx))

    # hypothesis rule
    hrs = makeRule(rule, addpbody=[use_atom])
    #warn('hrs: '+repr(hrs))
    main.append(hrs)

  main.append('#show.')
  main.append('#show usehr/1.')
  main.append('#show good_ex/1.')

  mains = '\n'.join(main)
  #warn('MAIN\n'+mains+'\n***')
  bests = getAnswersets(mains, args='--opt-mode=optN', withCost = True)
  debug('searching for hypotheses: found {} answer sets with costs {}'.format(
    len(bests), repr(map(lambda ans_cost: ans_cost[1], bests))))
  if len(bests) == 0:
    debug('learned no hypotheses')
    return []
  else:
    #warn(repr(bests))
    # last answer = best
    # second element of last answer set = optimal costs
    bestcosts = bests[-1][1]

    hypotheses = []
    for ans_cost in bests:
      atoms, cost = ans_cost
      if cost != bestcosts:
        # skip this, it is a suboptimal answerset
        continue
      # from atoms, get hypothesis structures
      hypothesis = []
      for atm in atoms:
        hr = rUseHR.match(atm)
        if hr:
          hr = int(hr.groups()[0])
          hypothesis.append(hyp[hr])
      hypotheses.append(sorted(hypothesis))
    # print nicely
    debug('learned hypotheses:')
    for idx, hyp in enumerate(hypotheses):
      debug(hypothesis2str(hyp, indent='  {} '.format(idx)))
    # output 
    return hypotheses

def hypothesis2str(hyp, indent=''):
  return '\n'.join(map(lambda x: indent+makeRule(x[0]), hyp))

def verifyAllExamplesWithHypothesis(task, hyp):
  # returns a tuple (allgood, quality)
  allgood = True
  goodcounter = 0
  for atex, ex in enumerate(task['examples']):
    if verifyHypothesisOnExample(task, ex, hyp):
      debug("example {} succeeded with hypothesis".format(ex['id']))
      goodcounter += 1
    else:
      debug("example {} failed with hypothesis".format(ex['id']))
      allgood = False
  quality = float(goodcounter) / len(task['examples'])
  return allgood, quality

try:
  import localconfig
  localconfig.configure(sys.modules[__name__])
except ImportError:
  pass

def testclingo():
  # get version from clingo
  cmd = '{} -v'.format(CLINGO)
  out = subprocess.check_output(cmd, shell=True)
  if 'clingo version 5' not in out:
    raise Exception('expected "clingo version 5" in output of '+repr(cmd))

def main():
  testclingo()
  argument = None
  if len(sys.argv) > 1:
    argument = sys.argv[1]
  if argument is None:
    argument = DEFAULTINPUTFILE

  # read input
  debug("reading input '{}'".format(argument))
  task = readTask(argument)
  if DEBUG:
    debugTask(task)

  if len(task['examples']) == 0:
    raise Exception('found no examples in input!')

  if len(task['tests']) == 0:
    raise Exception('found no tests in input!')

  # sort examples by trace length
  ex_s = sorted(task['examples'], key=lambda ex: len(ex['trace']))

  # key = example ID
  # value = list of hypotheses (a hypothesis is a list of rules+costs in structured representation)
  hypotheses = {}
  # key = (example ID, hypothesis idx)
  # value = quality measure (higher = better)
  quality = {}
  bestquality = 0.0

  # increase costlimit if we don't find results
  for costlimit in range(MINCOSTLIMIT, MAXCOSTLIMIT):
    # find smallest unsolved example
    for nextexidx in range(0,len(ex_s)):
      if ex_s[nextexidx]['id'] not in hypotheses:
        break

    ex = ex_s[nextexidx]

    # LEARN
    debug("LEARNING from ex {} with limit {}".format(ex['id'], costlimit))
    thishyps = learn(task, [ex], costlimit)
    if thishyps != []:
      hypotheses[ex['id']] = thishyps
    else:
      # try higher
      continue

    # check for each hypothesis how many example traces we can predict correctly
    # TODO if we sort the hypotheses by size we get more minimal results
    for hidx, hyp in enumerate(thishyps):
      # if all, we just leave here (we cannot learn more)
      allgood, q = verifyAllExamplesWithHypothesis(task, hyp)
      if allgood:
        debug("predicted all traces correctly with hypothesis {} of example {} -> leaving".format(hidx, ex['id']))

        # predict with this hypothesis
        attemptPredict('after {} examples at costlimit {}'.format(len(hypotheses), costlimit), task, hyp)
        return
      else:
        debug("hypothesis {} of example {} predicted traces with quality {}".format(hidx, ex['id'], q))

        # record quality
        quality[(ex['id'], hidx)] = q
        if q > bestquality:
          bestquality = q

          # predict with this hypothesis
          attemptPredict('after {} examples at costlimit {} with quality {}'.format(len(hypotheses), costlimit, q), task, hyp)
          # but continue, we still might find an "allgood" hypothesis

def makeVar(idx):
  return 'V{}'.format(idx)

def makeAtom(lst):
  if isinstance(lst, str) or isinstance(lst,unicode):
    return str(lst)
  pred = lst[0]
  if isinstance(pred, tuple):
    pred = '_'.join(pred)
  if len(lst) == 1:
    return str(pred)
  else:
    return "{}({})".format(pred, ','.join(map(str, lst[1:])))

def makeLiteral(lst,positive=True):
  if positive:
    return makeAtom(lst)
  else:
    return 'not '+makeAtom(lst)

#import atomparser

def getTuple(atmstr):
  out = []
  openbrackets = 0
  start = None
  ret = None
  for idx, char in enumerate(atmstr):
    if char == '(':
      if not start:
        # we went over the predicate successfully
        start = idx+1
        # record predicate
        out.append(atmstr[:idx])
      else:
        openbrackets += 1
    elif char == ')':
      assert(start)
      openbrackets -= 1
      if openbrackets == -1:
        # record last argument
        out.append(getTuple(atmstr[start:idx]))
        ret = tuple(out)
        break
    elif char == ',':
      assert(start)
      if openbrackets == 0:
        # record argument in argument list (not last)
        out.append(getTuple(atmstr[start:idx]))
        start = idx+1
  if ret is None:
    ret = atmstr
  #warn("from '{}' extracted {}".format(atmstr, repr(ret)))
  return ret

def makeVarSafe(item):
  varidx, type_ = item
  return (type_, makeVar(varidx))

def getRuleFromAnswerset(ras, structured=True, debugCosts=False):
  #warn('ras='+repr(ras))
  # this is the bottleneck XXX implement parser manually
  #ras = [atomparser.parse(a) for a in ras]
  ras = [getTuple(a) for a in ras]
  #warn('ras='+repr(ras))

  head = None
  pbodies = {}
  nbodies = {}
  variables = {}

  #for atm in ras:
  #  warn("processing {}".format(atm))

  predatoms = []
  varatoms = []
  totalcost = None
  detailedcosts = collections.defaultdict(list)
  for atm in ras:
    apred = atm[0]
    if atm[0] == 'totalcost':
      totalcost = int(atm[1])
    elif apred in ['use_head_pred', 'use_body_pred']:
      predatoms.append(atm)
    elif apred in ['use_var_type', 'bind_hvar', 'bind_bvar']:
      varatoms.append(atm)
    elif apred in ['cost'] and debugCosts:
      detailedcosts[atm[1]].append(atm[2:])

  # get predicates
  for atm in predatoms:
    apred = atm[0]
    aargs = atm[1:]
    if apred == 'use_head_pred':
      predid, pred, arity = aargs
      arity = int(arity)
      head = [pred] + [None]*arity
    elif apred == 'use_body_pred':
      id_idx, pred, polarity, arity = aargs
      arity = int(arity)
      dummy, predid, idx = id_idx
      if polarity == 'pos':
        bodies = pbodies
      else:
        bodies = nbodies
      bodies[tuple(id_idx)] = [pred] + [None]*arity
  
  # get variables
  for atm in varatoms:
    apred = atm[0]
    aargs = atm[1:]
    if apred == 'use_var_type':
      v_idx, type_ = aargs
      v, idx = v_idx
      variables[int(idx)] = type_
    elif apred == 'bind_hvar':
      pos, v_idx = aargs
      v, idx = v_idx
      head[int(pos)] = makeVar(idx)
    elif apred == 'bind_bvar':
      id_idx, polarity, position, v_idx = aargs
      dummy, predid, idx = id_idx
      v, idx = v_idx
      if polarity == 'pos':
        bodies = pbodies
      else:
        bodies = nbodies
      bodies[tuple(id_idx)][int(position)] = makeVar(idx)

  safevars = map(makeVarSafe, variables.iteritems())
  #warn('safevars:'+repr(safevars))

  if debugCosts:
    warn('detailedcosts:')
    for ct in detailedcosts.iterkeys():
      warn('  {}:'.format(ct))
      for c in detailedcosts[ct]:
        #warn('    '+' | '.join(map(repr, c)))
        warn('    '+' | '.join(map(makeAtom, c)))

  if structured:
    # structured output
    rule = {
      'head': head,
      'pbody':pbodies.values() + safevars,
      'nbody':nbodies.values()
    }

    return rule, totalcost
  else:
    # string output
    rule = makeAtom(head)
    if len(pbodies) + len(safevars) + len(nbodies) == 0:
      rule += '.'
    else:
      lits = sorted([ makeLiteral(lit, True) for lit in (pbodies.values()+safevars) ])
      lits += sorted([ makeLiteral(lit, False) for lit in nbodies.values() ])
      rule += " :- " + ",".join(lits) + "."
    ret = rule, totalcost
    if debugCosts:
      warn('[{}] {}\n'.format(totalcost, rule))
    return ret

if __name__ == '__main__':
  main()
