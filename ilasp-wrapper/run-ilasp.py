#!/usr/bin/python2.7

# ILASP Wrapper for Inductive Logic Programming Competition
# Copyright (C) 2017, 2018  Peter Schueller <schueller.p@gmail.com>
# Copyright (C) 2017  Mishal Kazmi <mishalkazmi@sabanciuniv.edu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, re, tempfile, subprocess, os, json, traceback, random, collections

# env

root = os.path.dirname(sys.argv[0])
if root == '':
  root = os.curdir
thisscript = os.getcwd()+'/'+sys.argv[0]

# config

CLINGO = 'clingo'
ILASP = 'ILASP'
ILASPPATH = None
# full path where to search for ILASP and where ILASP shall find its own clingo
#ILASPPATH = '/usr/local/bin:/usr/bin:/bin'
SOLVERARGS = ['--version=2i', '--clingo5', '-nc']

# input if no file is given as argument
DEFAULTINPUTFILE = '../examples/simple.task'

DEBUG = True
RMTEMPS = True

# regexes

rTimeDotsFact = re.compile(r'\s*time\(\s*([0-9]+)\s*\.\.\s*([0-9]+)\s*\).\s*')
rTimeFact = re.compile(r'\s*time\(\s*([0-9]+)\s*\).\s*')
rTimedAtom = re.compile(r'^([a-z][a-zA-Z_]*)\s*\(\s*(\(\s*[0-9]+\s*,\s*[0-9]+\s*\))\s*,\s*([0-9]+)\s*\)$')
rSymbolicAtom1 = re.compile(r'^([a-z][a-zA-Z_]*)\s*\(\s*([a-z][a-zA-Z_]*)\s*\)$')
rSymbolicAtom2 = re.compile(r'^([a-z][a-zA-Z_]*)\s*\(\s*([a-z][a-zA-Z_]*)\s*,\s*([a-z][a-zA-Z_]*)\s*\)$')
rUseHR = re.compile(r'^usehr\(([0-9]+)\)$')

def warn(msg):
  sys.stderr.write(str(msg)+'\n')

def debug(msg):
  if DEBUG:
    sys.stderr.write(str(msg)+'\n')

debug("running in root {} thisscript {}".format(root, thisscript))

def info(msg):
  sys.stdout.write(str(msg)+'\n')
  #sys.stderr.write(str(msg)+'\n')

def debugTask(task):
  # debug output
  for section in ['background', 'relevant_predicates', 'target_predicate']:
    debug('***{}***'.format(section))
    debug('  '+'\n  '.join(task[section]))
  for section in ['examples', 'tests']:
    debug('***{}***'.format(section))
    for ex in task[section]:
      debug('  #{}'.format(ex['id']))
      assert(ex['start'] == [])
      #debug('    TRACE\n      '+'\n      '.join(ex['trace']))
      debug('    TRACE '+' '.join(ex['trace']))
      if 'valid_moves' in ex:
        #debug('    VALID\n      '+'\n      '.join(ex['valid_moves']))
        debug('    VALID '+' '.join(ex['valid_moves']))

def readTask(infile):
  location = 'start'
  task = {
    'probabilistic' : False,
    'start' : [], # should stay empty
    'background' : [],
    'target_predicate' : [],
    'relevant_predicates' : [],
    'examples' : [],
    'tests' : [],
  }

  rRelPred = re.compile('#relevant_predicates (.*)')
  rExample = re.compile('#Example\((.*)\)')
  rTest = re.compile('#Test\((.*)\)')
  with open(infile, 'r') as inf:
    for fullline in inf:
      line = fullline.strip()
      if line == '':
        pass
      elif line == '#probabilistic':
        # Note that you only need to check this if your system is entering both
        # tracks (otherwise, you can assume you are only given tasks from your own
        # track)
        task['probabilistic'] = True
      elif line == '#background':
        location = 'background'
      elif line.startswith('#target_predicate'):
        location = 'target_predicate'
      elif line.startswith('#relevant_predicates'):
        location = 'relevant_predicates'
        rel = rRelPred.findall(line)
        #warn(repr('REL'+repr(rel)))
      elif line.startswith('#Example'):
        location = 'examples'
        sublocation = 'start'
        exid = rExample.findall(line)
        #warn(repr('EX'+repr(exid)))
        task['examples'].append({
          'id': exid[0],
          'start': [], # should stay empty
          'trace': [],
          'valid_moves': [],
        })
      elif line.startswith('#trace'):
        sublocation = 'trace'
      elif line.startswith('#valid_moves') and location == 'examples':
        sublocation = 'valid_moves'
      elif line.startswith('#Test'):
        location = 'tests'
        sublocation = 'trace'
        testid = rTest.findall(line)
        task['tests'].append({
          'id' : testid[0],
          'start': [], # should stay empty
          'trace' : [],
        })
      else:
        if location == 'tests' or location == 'examples':
          #warn('appending to '+repr(task[location][-1][sublocation]))
          #warn('appending '+repr(line))
          task[location][-1][sublocation].append(line.strip('.'))
        else:
          task[location].append(line)

  assert(not task['probabilistic'])
  # for probabilistic:
  # puts "VALID(#{trace[:id]}, #{rand(101).to_f/100.0})"

  # otherwise there is junk at the beginning
  assert(task['start'] == [])

  # parser assertion
  for section in ['examples', 'tests']:
    for ex in task[section]:
      assert(ex['start'] == [])
  assert(task['start'] == [])

  return task

def getAnswersets(program, args='0', withCost=False):
  '''
  default arguments: get all answer sets
  '''
  #warn('getAnswersets with program:\n***\n{}\n***'.format(program))
  tf = tempfile.NamedTemporaryFile(prefix='insplp',delete=False)
  tf.write(program)
  tf.close()
  debug('wrote ASP to {}'.format(tf.name))
  clingoproc = subprocess.Popen('{} --outf=2 {} {}'.format(CLINGO, args, tf.name), shell=True, stdin=None, stdout=subprocess.PIPE)
  outs, err = clingoproc.communicate(None)
  if RMTEMPS:
    os.remove(tf.name)
  #warn('FROMCLINGO:\n'+outs)
  out = json.loads(outs)
  if 'Witnesses' in out['Call'][0]:
    answersets = out['Call'][0]['Witnesses']
    if 'Optimal' in out['Models']:
      # if we have N optimal models, these are the N last witnesses
      answersets = answersets[-out['Models']['Optimal']:]
    #warn('FROMCLINGO:\n'+repr(out))
    if withCost:
      return [ (witness['Value'], witness['Costs']) for witness in answersets]
    else:
      return [witness['Value'] for witness in answersets]
  else:
    return []


def attemptPredict(infostr, task, hypstr):
  debug('prediction attempt {} with hypothesis:\n{}'.format(infostr, hypstr))
  # You can do this multiple times. Only your final attempt will be scored.
  info("#attempt")

  for test in task['tests']:
    traceid = test['id']
    trace = test['trace']

    testprog = []
    # copy background into testprog, do not modify background!
    testprog += task['background']
    testprog.append(hypstr)
    # trace
    testprog.append('\n'.join(map(lambda t: t+'.', trace)))
    # test trace
    testprog.append('invalid_at(Time) :- agent_at(Cell,Time), not valid_move(Cell,Time-1), time(Time-1), time(Time).')
    testprog.append('invalid :- invalid_at(Time).')
    testprog.append('#show invalid/0.')

    testprogs = '\n'.join(testprog)
    #warn('evaluating trace {} in testprog\n***\n{}\n***'.format(traceid, testprogs))
    answersets = getAnswersets(testprogs, args='1')
    assert(len(answersets) > 0)
    first = answersets[0]

    if 'invalid' in first:
      info("INVALID({})".format(traceid))
    else:
      info("VALID({})".format(traceid))

def parseTimes(background):
  times = set()
  for bg in background:
    #warn('parseTimes bg:'+repr(bg))
    m = rTimeDotsFact.match(bg)
    if m:
      #warn('parseTimes:'+repr(m.groups()))
      timelow, timeup = m.groups()
      times.add(int(timelow))
      times.add(int(timeup))
    m = rTimeFact.match(bg)
    if m:
      #warn('parseTimes:'+repr(m.groups()))
      time = m.groups()
      times.add(int(time))
  return min(times), max(times)

def learn(task, examples, args):

  ILASPinput = []
  ILASPinput += convertNonExample(task)
  for ex in examples:
    ILASPinput += convertExample(task, ex)
  ILASPinput = ''.join(ILASPinput)

  # call ILASP
  tfi = tempfile.NamedTemporaryFile(prefix='ilaspinp', suffix='.las',delete=False)
  tfi.write(ILASPinput)
  tfi.close()
  debug('wrote ILASP input to {}'.format(tfi.name))
  tfo = tempfile.NamedTemporaryFile(prefix='ilaspout', suffix='.txt',delete=False)
  myenv = os.environ.copy()
  if ILASPPATH:
    # override
    myenv['PATH'] = ILASPPATH
  subprocess.call('{} {} {} 2>&1 |tee {} >&2'.format(ILASP, ' '.join(args), tfi.name,tfo.name),shell=True,env=myenv)
  debug('wrote ILASP output to {}'.format(tfo.name))

  # return hypothesis as a string
  Hyp_proc = subprocess.Popen('grep -v "runlim\|Pre\|Solve\|Total" {}'.format(tfo.name) ,shell=True,
                              stdin=None, stdout=subprocess.PIPE)
  out, err = Hyp_proc.communicate()
  hypothesis = out
  if 'error' in hypothesis:
    raise Exception("got error in hypothesis: "+hypothesis)

  debug("got hypothesis: "+hypothesis)
  return hypothesis

def _to_mode(line, chars):
    """
    Converts a line into mode format.

    Parameters
    ----------
    line: str - line which should be converted.
    chars: str - chars following after "mode" and before "(", e.g. for "modeh(",
    chars = "h".

    Returns
    -------
    str.
    Formatted line.

    """
    tmp = line.split("(")
    name = tmp[0] + "("
    # Remove the ")" at the end
    vars = tmp[1][:-1].split(",")
    for var in vars:
        name += "var(" + var.strip() + "),"
    # Remove last comma
    name = name[:-1] + ")"
    extra = ''
    if name in ['adjacent', 'wall']:
      extra = ',(anti_reflexive)'
    maxusage = 1
    name = "#mode" + chars + "("+str(maxusage)+"," + name + extra +").\n"
    return name

def convertNonExample(task):
  ilaspinp = []

  # parse min/max time in background knowledge
  timelow, timeup = parseTimes(task['background'])
  # background knowledge
  for line in task['background']:
    # do not output time from background (will do per example)
    if not line.startswith('time('):
      ilaspinp.append(line + '\n')

  #Background
  ilaspinp.append('\n\n#maxv(3).')
  ilaspinp.append('\n#max_penalty(100).\n')

  #Mode Head
  ilaspinp.append('\n')
  modehs = (_to_mode(t,'h') for t in task['target_predicate'])
  ilaspinp += modehs

  #Mode Body
  ilaspinp.append('\n')
  modebs = (_to_mode(t,'b') for t in task['relevant_predicates'])
  ilaspinp += modebs

  # print time
  ilaspinp.append("\ntime({mi}..{ma}).\n".format(mi=timelow, ma=timeup))

  # print ilaspinp
  return ilaspinp

def convertExample(task, ex):
  warn("converting example "+repr(ex))
  ilaspinp = []

  id = ex['id']
  trace = ex['trace']
  valid = ex['valid_moves']

  # positive
  ilaspinp.append("\n#pos(ex" + id + ", {\n" + ',\n'.join(valid) + '\n},\n')

  # negative
  neg = createNegativeExample(task,ex)
  ilaspinp.append('{\n' + ',\n'.join(neg) + '\n},\n')

  # context
  ilaspinp.append('{\n' + '\n'.join([ x+'.' for x in trace]) + '\n}).\n')

  return ilaspinp

def createNegativeExample(task,ex):
  aspprog = []

  trace = ex['trace']
  valid = ex['valid_moves']
  negex = []

  # background knowledge
  aspprog += task['background']
  # trace facts from examples
  aspprog.append('\n'.join(map(lambda t: t+'.', trace)))
  # valid_moves facts from examples
  aspprog.append('\n'.join(map(lambda t: t+'.', valid)))

  aspprog.append('invalid_move(C,T) :- agent_at(_,T), cell(C), not valid_move(C, T).')
  aspprog.append('#show invalid_move/2.')

  aspprog = '\n'.join(aspprog)

  answersets = getAnswersets(aspprog, args='1')
  assert(len(answersets) > 0)

  for answer in answersets:
    for atom in answer:
      atom = atom.replace('invalid', 'valid')
      negex.append(atom)

  # print negex
  return negex

def main():
  global SOLVERARGS
  argument = None
  if len(sys.argv) == 2:
    argument = sys.argv[1]
  elif len(sys.argv) >= 3:
    argument = sys.argv[1]
    SOLVERARGS = sys.argv[2:]
  if argument is None:
    argument = DEFAULTINPUTFILE
  sys.stderr.write("running with argument {} and solverargs {}\n".format(repr(argument), repr(SOLVERARGS)))

  # read input
  debug("reading input '{}'".format(argument))
  task = readTask(argument)
  if DEBUG:
    debugTask(task)

  if len(task['examples']) == 0:
    raise Exception('found no examples in input!')

  if len(task['tests']) == 0:
    raise Exception('found no tests in input!')

  # sort examples by trace length
  ex_s = sorted(task['examples'], key=lambda ex: len(ex['trace']))

  # learn on example ex_s[0]
  hypothesis = learn(task, ex_s, SOLVERARGS)

  # predict with this hypothesis
  attemptPredict('first attempt', task, hypothesis)

main()
