# Inspire Inductive Logic Programming System #

This repository contains the "Inspire" Inductive Logic Programming (ILP) System that was submitted to the inaugural competition on Inductive Logic Programming 2016.

The main responsible is **Peter Schüller** http://www.peterschueller.com/ http://www.knowlp.com/

# Installation #

* Download Clingo version 5.2.2 (older versions will not work)

  https://github.com/potassco/clingo/releases

  Precompiled versions are fine, it is **not** necessary to have a Python-aware version.

  Clingo should be reachable in the PATH of the system as `clingo`.

* Test the system

  From the root directory, either run
  `./system/inspire-ilp.py example/simple.task`
  or
  `python system/inspire-ilp.py examples/simple.task`

  The result should be a lot of verbose output and at the end a learned hypothesis and a prediction of valid/invalid test examples (the competition output).

* If desired, and if you have ILASP installed, you can also test the ILASP wrapper

  From the root directory, either run
  `./ilasp-wrapper/run-ilasp.py example/simple.task`
  or
  `python ilasp-wrapper/run-ilasp.py example/simple.task`

  The result should also be a learned hypothesis and a prediction of valid/invalid test examples (the competition output).

# Configuration of Inspire #

The following configuration options can be changed by editing the beginning of file `inspire-ilp.py`.

* The path/name for Clingo 5 executable is set in variable `CLINGO_BIN`.
* The cost limits for iterative search are set in `MINCOSTLIMIT` and `MAXCOSTLIMIT`.
* The path of the hypothesis generation script is set in `HYPGENLP`.
* Whether to print the hypothesis is configured in `PRINTHYPOTHESIS`.
* Whether to print verbose output is configured in `VERBOSE`.
* Whether to print debug output is configured in `DEBUG`.
* Whether to delete temporary files (ASP programs) is configured in `RMTEMPS`. Paths of these files are printed in debug output.

# Configuration of ilasp-wrapper #

The ILASP wrapper can also be configured by editing
variables at the beginning of its python file.

We tested with ILASP-3.1.0 which also requires Clingo 5.

# Links #

* Inaugural Inductive Logic Programming Competition 2016

  http://ilp16.doc.ic.ac.uk/competition/

# Maintainer #

* Peter Schüller <schueller.p@gmail.com>
* Technische Universität Wien, Institut für Logic and Computation, Austria
* KnowLP Research Group http://www.knowlp.com/

# Contributors #

* Peter Schüller (2016-2018)
* Mishal Kazmi (2017)

# Acknowledgements #

This project has been supported by The Scientific and Technological Research Council of Turkey (TUBITAK) under grant agreement 114E777.

